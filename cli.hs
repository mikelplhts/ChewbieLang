module ChewbieLang.Interface where

import ChewbieLang
import System.IO
import System.Console.ANSI
import Data.Char

getPrintableValue :: String -> String
getPrintableValue v = case v of
  "True"  -> "true"
  "False" -> "false"
  v       -> v

repeatString :: String -> Int -> String
repeatString xs 0 = []
repeatString xs n = xs ++ (repeatString xs (n-1))

underlineError :: String -> String -> IO ()
underlineError inp err = do
  putStr ("    " ++ (take (length inp - length err) inp))
  setSGR [SetColor Foreground Vivid Red]
  putStrLn err
  putStr   ("    " ++ (repeatString " " (length inp - length err)))
  putStrLn (repeatString "^" (length err))
  setSGR [Reset]
  

getMemory :: Env -> String
getMemory [] = "------------------\nEnd of the memory."
getMemory (e:es) = (getName e) ++ " = " ++ getValue e ++ " :: " ++ show (getType e) ++ "\n" ++ (getMemory es)

doParse :: Env -> IO ()
doParse env = do
  putStr ">> "
  hFlush stdout
  inp <- getLine

  case inp of
    ":exit" -> putStrLn "Grrrrrwwwwwrr!"
    ":help" -> do
      putStrLn "This is the help command utility. This are the commands available for CLI (Chewbie Lang Interface):"
      putStrLn "  :exit       Exit the CLI program."
      putStrLn "  :help       Show this help utility."
      putStrLn "  :authors    Show the authors of this program."
      putStrLn "  :version    Show the version of the language."
      putStrLn "  :memory     Show the current memory of the language."
      putStrLn "  :clear      Clear the memory."
      doParse env
    ":authors" -> do
      putStrLn "The authors of the ChewbieLang language and related programs are:"
      putStrLn " - Michele Lapolla"
      putStrLn " - Vincenzo Digeno"
      doParse env
    ":version" -> do
      putStrLn ("ChewbieLang, version " ++ ChewbieLang.version)
      doParse env
    ":memory" -> do
      putStrLn $ getMemory env
      doParse env
    ":clear" -> do
      putStrLn $ "Memory cleared."
      doParse []
    (':':xs) -> do
      putStrLn ("Unknown command \":" ++ xs ++ "\".")
      putStrLn "Type :help to show the command list."
      doParse env
    otherwise -> case parse statement env inp of
      []          -> do
        putStrLn "Syntax error: cannot parse the whole input.\n"
        underlineError inp inp
        doParse env
      [(es,v,"")] -> do
        putStrLn $ getPrintableValue v
        doParse es
      [(es,v,xs)] -> do
        putStrLn "Syntax error: cannot consume the input until the end.\n"
        underlineError inp xs
        doParse es

main :: IO ()
main = do
  hSetBuffering stdin NoBuffering
   
  putStrLn ("ChewbieLang, version " ++ ChewbieLang.version)
  putStrLn "Made by Michele Lapolla and Vincenzo Digeno"
  putStrLn "Type \":exit\" to quit the program or \":help\" to show the help."
  
  doParse []
