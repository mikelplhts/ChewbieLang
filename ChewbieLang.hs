module ChewbieLang where

import Control.Applicative
import Control.Monad
import Data.Char
import System.IO

version = "1.0.0" :: String

-- | The datatypes of the language.
data DataType
        = IntDataType
        | BoolDataType
        | FloatDataType
        | CharDataType
        deriving (Show, Enum, Eq)

-- | The single environment object.
type EnvObj = (String, DataType, String)

-- | The list of all environmnet objcts.
type Env = [EnvObj]

newtype Parser a = P (Env -> String -> [(Env, a, String)])

instance Functor Parser where
  -- fmap :: (a -> b) -> Parser a -> Parser b
  fmap g p = P (\env inp -> case parse p env inp of
                  [] -> []
                  [(newenv, v, out)] -> [(newenv, g v, out)]) 

instance Applicative Parser where
  -- pure :: a -> Parser a
  pure v = P (\env inp -> [(env, v, inp)])

  -- <*> :: Parser (a -> b) -> Parser a -> Parser b
  pg <*> px = P (\env inp -> case parse pg env inp of
                   [] -> []
                   [(env, g, out)] -> parse (fmap g px) env out)

instance Alternative Parser where
  -- empty :: Parser b
  empty = P (\env inp -> [])

  -- <|> :: Parser a -> Parser a -> Parser a
  p <|> q = P (\env inp -> case parse p env inp of
                 [] -> parse q env inp  
                 [(env, v, out)] -> [(env, v, out)])

instance Monad Parser where
  -- return :: a -> Parser a
  return v = P (\env inp -> [(env, v, inp)])

  -- >>= :: Parser a -> (a -> Parser b) -> Parser b
  p >>= f = P (\env inp -> case parse p env inp of
                 [] -> []
                 [(newenv, v, out)] -> parse (f v) newenv out)

parse :: Parser a -> Env -> String -> [(Env, a, String)]
parse (P p) env inp = p env inp

-- | Gets the name from a specific environment object.
getName :: EnvObj -> String
getName (n,_,_) = n

-- | Returns the datatype of a specific environment object.
getType :: EnvObj -> DataType
getType (_,t,_) = t

-- | Returns the value of a specific environment object.
getValue :: EnvObj -> String
getValue (_,_,v) = v

-- | Set or update the state of the environment
setEnv :: EnvObj -> Env -> Env
setEnv o [] = [o]
setEnv o (e:es)
            | (getName e) == (getName o) = [o] ++ es
            | otherwise                  = e:(setEnv o es)

-- | Returns the environment from a parser output.
getEnv :: [(Env, a, String)] -> Env
getEnv [] = []
getEnv [(es,_,_)] = es

-- | Returns the parser changing the state of the environment
returnSetEnv :: EnvObj -> Parser String
returnSetEnv o = P (\env inp -> [(setEnv o env, getValue o, inp)])

-- | Lookup for a variable with a specific name and type
lookupVar :: Env -> String -> DataType -> [EnvObj]
lookupVar [] n t = []
lookupVar (e:es) n t
              | getName e == n && getType e == t   = [e]
              | otherwise                          = lookupVar es n t

-- | Read a variable with a specific name and type
readVar :: String -> DataType -> Parser String
readVar n t = P (\env inp -> case lookupVar env n t of
                               [] -> []
                               [(_,_,v)] -> [(env,v,inp)])

--  | Add commands to the input string
--    It takes the new input to parse and concatenete it on the original input
extendInput :: String -> Parser String
extendInput i = P (\env inp -> [(env, "", i ++ inp)])

--  | The 'item' function is the parser that retrieves the next character of
--    the input.
item :: Parser Char
item = P (\env inp -> case inp of 
            [] -> []
            (x:xs) -> [(env, x, xs)])

-- | The 'sat' function is a parser that checks if the following parsed item
--   is a specific character.
--   It takes one argument, of type Char.
sat :: (Char -> Bool) -> Parser Char
sat p = do
          x <- item 
          if p x
            then return x
            else empty

-- | Removes the spaces and returns only the parsed value.
--   It take one argument, of type Parser.
token :: Parser a -> Parser a
token p = do
            many space
            v <- p
            many space
            return v

-- | Removes the white spaces and returns only the parsed value.
tokenWS :: Parser a -> Parser a
tokenWS p = do
              many white_space
              v <- p
              many white_space
              return v

-- #
-- # Command parsers
-- #

statement :: Parser String
statement = do
              c <- command 
              t <- some $ token cmdterm
              s <- statement
              return (c ++ ['\n'] ++ s)
            <|>
            do
              c <- command
              t <- many $ token cmdterm
              return c

command :: Parser String
command = while <|> ifthenelse <|> assignment <|> skip <|> identity

while :: Parser String
while =  do
           w <- syntactic_while
           extendInput w
           string "while"
           b <- bexpr
           token $ string "do"
           if b then 
            do
             statement
             string "endwhile"
             extendInput w
             while
           else
            do
             syntactic_statement
             string "endwhile"
             return empty

ifthenelse :: Parser String
ifthenelse = do
               token (string "if")
               b <- bexpr
               token (string "then") 
               if b then
                do 
                  s <- tokenWS statement
                  token (string "else")
                  ss <- tokenWS syntactic_statement
                  token (string "endif")
                  return s
                <|>
                do 
                  s <- tokenWS statement
                  token (string "endif")
                  return s
               else
                do
                  ss <- tokenWS syntactic_statement
                  token (string "else")
                  s <- tokenWS statement
                  token (string "endif")
                  return s
                <|>
                do 
                  ss <- tokenWS syntactic_statement
                  token (string "endif")
                  return empty              

assignment :: Parser String
assignment = 
 do { i <- token identifier;
      token $ char '=';
      do { be <- bexpr;
           returnSetEnv (i, BoolDataType, show(be)) }
      <|>
      do { ae <- aexpr;
           returnSetEnv (i, rightType ae, show(ifIntThenConvert ae)) }
      <|>
      do { cc <- char_const;
           returnSetEnv (i, CharDataType, [cc]) } }
 where rightType t = if isInteger t then IntDataType else FloatDataType
       ifIntThenConvert t = if t == (fromIntegral $ floor t) then  fromIntegral $ floor t else t  

-- | The skip parser skips the next command
skip :: Parser String
skip = do
         token (string "skip")
         syntactic_command 
         return empty
       <|>
       do
         token (string "skip")
         return empty
         
identity :: Parser String
identity = do
             token (string "id") 
             e <- expr
             return e
           <|>
             expr
      
chainl :: Parser a -> Parser (a -> a -> a) -> Parser a
p `chainl` pop = p >>= rest
  where rest x = next x <|> return x
        next x = do
                   o <- pop
                   y <- p
                   rest $ x `o` y

parenP :: Char -> Parser a -> Char -> Parser a
parenP l p r = do
                 token $ char l
                 x <- p
                 token $ char r
                 return x

-- #
-- # EXPRESSION PARSERS
-- #

-- | Generic expression
expr :: Parser String
expr = do
         b <- bexpr
         return $ show b
       <|>
       do
         a <- aexpr
         return $ show a

-- | Arithmetic expressions.

aexpr :: Parser Float
aexpr = aterm `chainl` addOp

aterm :: Parser Float
aterm = afactor `chainl` mulOp

afactor :: Parser Float
afactor = parenP '(' aexpr ')'
          <|>
          do
            s <- sign
            a <- aexpr 
            return $ s * a
          <|>
          do
            i <- identifier
            a <- token $ readVar i FloatDataType
            return (read a :: Float)
          <|>
          do
            i <- identifier
            a <- token $ readVar i IntDataType
            return (read a :: Float)
          <|>
          token float_const
          <|>   
          token int_const 

bexpr :: Parser Bool
bexpr = bterm `chainl` orOp

bterm :: Parser Bool
bterm = bfactor `chainl` andOp

bfactor :: Parser Bool
bfactor = parenP '(' bexpr ')' 
          <|>
          do
            token $ char '!'
            b <- bfactor
            return $ not b
          <|>
          do
            a1 <- aexpr
            op <- compareOp
            a2 <- aexpr
            return $ a1 `op` a2
          <|>
          token bool_const
          <|>
          do
            i <- identifier
            b <- token $ readVar i BoolDataType
            return (read b :: Bool)

-- #
-- # Language operations
-- #

addOp :: Fractional a => Parser (a -> a -> a)
addOp = plus <|> minus
        where plus  = char '+' >> return (+)
              minus = char '-' >> return (-)
mulOp :: Fractional a => Parser (a -> a -> a)
mulOp = times <|> divide
        where times  = char '*' >> return (*)
              divide = char '/' >> return (/)

orOp :: Parser (Bool -> Bool -> Bool)
orOp = string "||" >> return (||)

andOp :: Parser (Bool -> Bool -> Bool)
andOp = string "&&" >> return (&&)

compareOp :: Ord a => Parser (a -> a -> Bool)
compareOp = greaterOrEqualThan <|> greaterThan <|> lessOrEqualThan
              <|> lessThan <|> equalTo <|> notEqualTo
  where greaterOrEqualThan = string ">=" >> return (>=)
        greaterThan        = char   '>'  >> return (>)
        lessOrEqualThan    = string "<=" >> return (<=)
        lessThan           = char   '<'  >> return (<)
        equalTo            = string "==" >> return (==)
        notEqualTo         = string "!=" >> return (/=)

isNotKeyword :: String -> Bool
isNotKeyword "endwhile" = False
isNotKeyword "endif" = False
isNotKeyword "if" = False
isNotKeyword "then" = False
isNotKeyword "else" = False
isNotKeyword "do" = False
isNotKeyword _ = True     

isNonzeroDigit :: Char -> Bool
isNonzeroDigit c = c >= '1' && c <= '9' 

digit :: Parser Char
digit = sat isDigit

nonzero_digit :: Parser Char
nonzero_digit = sat isNonzeroDigit

isInteger :: Float -> Bool
isInteger n = (n -  fromIntegral (floor n)) == 0

lower :: Parser Char
lower = sat isLower

upper :: Parser Char
upper = sat isUpper

letter :: Parser Char
letter = sat isAlpha

nondigit :: Parser Char
nondigit = letter <|> sat (== '_')

symbol :: Parser Char
symbol = sat isSymbol

alphanum :: Parser Char
alphanum = letter <|> digit <|> symbol

char :: Char -> Parser Char
char x = sat (== x)

string :: String -> Parser String
string [] = return []
string (x:xs) = do 
                  char x
                  string xs
                  return (x:xs)

sign :: Parser Float
sign = positive <|> negative
  where positive = char '+' >> return ( 1)
        negative = char '-' >> return (-1)

compareop :: Parser String
compareop = string ">=" <|> string "<=" <|> string "==" <|> string ">" <|> string "<" <|> string "!=" 

-- |
-- | Constants of the lanugae
-- |

string_const :: Parser String
string_const = do
                 char '\"'
                 str <- many alphanum
                 char '\"'
                 return str

char_const :: Parser Char
char_const = do
               char '\''
               c <- alphanum
               char '\''
               return c

int_const :: Parser Float
int_const = do
              xs <- some digit
              return (read xs)

-- | The 'float_const' function is the parser for floating constants.
float_const :: Parser Float
float_const = do
                xs <- many digit
                char '.'
                fs <- some digit
                if xs == ""
                  then return (read ("0." ++ fs))
                  else return (read (xs ++ "." ++ fs))

-- | The 'bool_const' function is the parser for boolean constants.
bool_const :: Parser Bool
bool_const = true <|> false
  where true  = string "true"  >> return True
        false = string "false" >> return False

-- | The 'isSimpleSpace' functions checks if a character is a simple space,
--   that is, a space but not a new line.
isSimpleSpace :: Char -> Bool
isSimpleSpace c = isSeparator c || c == '\t'

-- | The 'space' function is the parser for simple spaces.
space :: Parser Char
space = sat isSimpleSpace

-- | The 'white_space' function is the parser for generic spaces (also new line).
white_space :: Parser Char
white_space = sat isSpace

-- The 'cmdterm' function is the parser for command terminators.
cmdterm :: Parser Char
cmdterm = char ';'


-- The 'identifier' function is the parser for identifier (e.g., variables)
identifier :: Parser String
identifier = do
               x <- nondigit
               xs <- some (nondigit <|> digit)
               if isNotKeyword (x:xs)
                 then return (x:xs)
                 else empty
             <|>
             do
               x <- letter
               xs <- many (nondigit <|> digit)
               if isNotKeyword (x:xs)
                 then return (x:xs)
                 else empty

-- #
-- # Syntactic Parsers
-- #
--

syntactic_sign :: Parser String
syntactic_sign = string "-" <|> string "+"

syntactic_int_const :: Parser String
syntactic_int_const = do
                        xs <- some digit
                        return xs

syntactic_float_const :: Parser String
syntactic_float_const = do
                          xs <- many digit
                          char '.'
                          fs <- some digit
                          if xs == ""
                            then return ("0." ++ fs)
                            else return (xs ++ "." ++ fs)

syntactic_bool_const :: Parser String
syntactic_bool_const = token (string "true") <|> token (string "false")

syntactic_statement = do
                        c <- syntactic_command
                        ct <- some $ token cmdterm
                        s <- syntactic_statement
                        return (c ++ ct ++ s)
                      <|>
                      do
                        c <- syntactic_command
                        ct <- many $ token cmdterm
                        return (c ++ ct)

syntactic_command :: Parser String
syntactic_command = syntactic_while <|> syntactic_ifthenelse <|> syntactic_assignment <|> syntactic_skip <|> syntactic_identity

syntactic_while :: Parser String
syntactic_while = do
                    k1 <- token (string "while")
                    b <- syntactic_bexpr
                    k2 <- token (string "do")
                    ss <- tokenWS syntactic_statement
                    k3 <- token (string "endwhile")
                    return ("while " ++ b ++ " do " ++ ss ++ " endwhile") 

syntactic_ifthenelse :: Parser String
syntactic_ifthenelse = do
                         k1 <- token (string "if")
                         b <- syntactic_bexpr
                         k2 <- token (string "then")
                         s1 <- tokenWS syntactic_statement
                         k3 <- (string "else")
                         s2 <- tokenWS syntactic_statement
                         k4 <- token(string "endif")
                         return (k1 ++ b ++ k2 ++ s1 ++ k3 ++ s2 ++ k4)
                       <|>
                       do
                         k1 <- token (string "if")
                         b <- syntactic_bexpr
                         k2 <- token (string "then")
                         s1 <- tokenWS syntactic_statement
                         k3 <- token(string "endif")
                         return (k1 ++ b ++ k2 ++ s1 ++ k3)                     
        
syntactic_assignment :: Parser String
syntactic_assignment =
    do { i  <- token identifier;
         c <- token $ char '=';
         
         do { b <- syntactic_bexpr;
              return (i ++ [c] ++ b); }
         <|>
         do { a <- syntactic_aexpr;
              return (i ++ [c] ++ a); } }

syntactic_skip :: Parser String
syntactic_skip = do
                   s <- token (string "skip")
                   c <- syntactic_command
                   return (s ++ c)
                 <|>
                 do
                   s <- token (string "skip")
                   return s

syntactic_identity :: Parser String
syntactic_identity = do
                       k <- token (string "id")
                       e <- syntactic_expr
                       return (k ++ " " ++ e)
                     <|>
                       syntactic_expr

syntactic_expr :: Parser String
syntactic_expr = syntactic_bexpr <|> syntactic_aexpr  

syntactic_aexpr :: Parser String
syntactic_aexpr = do
                    a1 <- syntactic_aterm
                    add <- char '+'
                    a2 <- syntactic_aexpr
                    return (a1 ++ [add] ++ a2)
                  <|>
                  do
                    a1 <- syntactic_aterm
                    min <- char '-'
                    a2 <- syntactic_aexpr
                    return (a1 ++ [min] ++ a2)
                  <|>
                    syntactic_aterm

syntactic_aterm :: Parser String
syntactic_aterm = do
                    a1 <- syntactic_afactor
                    mul <- char '*'
                    a2 <- syntactic_aterm
                    return (a1 ++ [mul] ++ a2)
                  <|>
                  do
                    a1 <- syntactic_afactor
                    div <- char '/'
                    a2 <- syntactic_aterm
                    return (a1 ++ [div] ++ a2)
                  <|>
                    syntactic_afactor

syntactic_afactor :: Parser String
syntactic_afactor =  do
                       op <- token (char '(')
                       ae <- syntactic_aexpr
                       cp <- token (char ')')
                       return ([op] ++ ae ++ [cp])
                     <|>
                     do
                       s <- syntactic_sign
                       ic <- syntactic_aexpr
                       return (s ++ ic)
                     <|> 
                     syntactic_float_const
                     <|>
                     syntactic_int_const 
                     <|>
                     do {
                       i <- identifier;
                       do {
                         a <- readVar i IntDataType;
                         return i;
                       }
                       <|>
                       do {
                         a <- readVar i FloatDataType;
                         return i;
                       }
                     }

syntactic_bexpr :: Parser String
syntactic_bexpr = do
                    b1 <- syntactic_bterm
                    or <- token (string "||")
                    b2 <- syntactic_bexpr
                    return (b1 ++ or ++ b2)
                  <|>
                    syntactic_bterm

syntactic_bterm :: Parser String
syntactic_bterm = do
                    b1 <- syntactic_bfactor
                    and <- token (string "&&")
                    b2 <- syntactic_bterm
                    return (b1 ++ and ++ b2)
                  <|>
                    syntactic_bfactor

syntactic_bfactor :: Parser String
syntactic_bfactor =  do
                       op <- token (char '(')
                       be <- syntactic_bexpr
                       cp <- token (char ')')
                       return ([op] ++ be ++ [cp])
                     <|>
                     do
                       n <- token (char '!')
                       be <- syntactic_bfactor
                       return ([n] ++ be)
                     <|>
                     do
                       a1 <- syntactic_aexpr
                       cp <- token compareop
                       a2 <- syntactic_aexpr
                       return (a1 ++ cp ++ a2)
                     <|>
                     token syntactic_bool_const
                     <|>
                     do
                       i <- identifier
                       b <- readVar i BoolDataType
                       return i 


--check :: [(Env, String, String)] -> String
--check [] = "Error while parsing the input string."
--check [(e,v,[])] = "res = " ++ show v ++ "\nenv = " ++ show e
--check [(_,_,inp)] = "Error: unparsed input \"" ++ inp ++ "\""
